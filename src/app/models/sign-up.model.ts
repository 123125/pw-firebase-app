export class SignUpFormModel {
  username: string = null;
  email: string = null;
  password: string = null;
};
