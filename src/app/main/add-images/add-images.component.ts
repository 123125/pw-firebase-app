import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { ImagesService } from 'src/app/services/images.service';

@Component({
  selector: 'app-add-images',
  templateUrl: './add-images.component.html',
  styleUrls: ['./add-images.component.scss']
})
export class AddImagesComponent implements OnInit {

  myImages: File[] = [];

  @ViewChild('filesInput') filesInput: ElementRef;

  constructor(private storage: AngularFireStorage, private imagesService: ImagesService) { }

  ngOnInit(): void {}

  onFileChange(event) {  
    for (var i = 0; i < event.target.files.length; i++) { 
        this.myImages.push(event.target.files[i]);
    }
    this.filesInput.nativeElement.value = null;
  }

  addImages() {
    from(this.myImages).pipe(
      concatMap(async image => {
        // const task = this.firebaseService.saveImageToStorage(image);
        const ref = this.storage.storage.ref(`images/${Date.parse(new Date().toString()) + '-' + image.name}`);
        return ref.put(image, { customMetadata: { name: image.name, type: image.type } }).then(async snapshot => {
          const fullLink = await snapshot.ref.getDownloadURL();
          return {
            fullPath: snapshot.ref.fullPath,
            bucket: snapshot.ref.bucket,
            name: snapshot.ref.name,
            fullLink
          };
        });
      }),
      concatMap(image => this.imagesService.addImage(image))
    ).subscribe(() => this.myImages = []);
  }

  deleteImageFromMyImages(idx: number){
    Array.from(this.filesInput.nativeElement.files).splice(idx, 1);
    this.myImages.splice(idx, 1);
  }

}
