import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessagingService } from './services/messaging.sevice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  isLoggedOut$: Observable<boolean>;

  pictureUrl$: Observable<string>;

  constructor(
    private auth: AngularFireAuth,
    private router: Router,
    private messagingService: MessagingService
  ) {}

  ngOnInit() {
    this.messagingService.requestPermission();
    
    this.isLoggedIn$ = this.auth.authState.pipe(map(user => !!user));

    this.isLoggedOut$ = this.isLoggedIn$.pipe(map(user => !user));
  }

  logout() {
    this.auth.auth.signOut().then(() => this.router.navigateByUrl('/auth/sign-in'));
  }
 
}
