export interface IUserInfo {
    balance: number;
    email: string;
    id?: number;
    username: string;
};
  