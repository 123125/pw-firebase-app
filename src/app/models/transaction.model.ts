export interface ITransaction {
    date: string,
    username: string, 
    amount: number,
    balance: number,
    userId?: string,
    recipientId?: string
};