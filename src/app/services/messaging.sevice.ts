import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MessagingService {

    constructor(private angularFireMessaging: AngularFireMessaging) {
        if (navigator && navigator.serviceWorker) {
            navigator.serviceWorker.addEventListener('message', this.receiveMessage.bind(this));
        }
    }

    requestPermission() {
        this.angularFireMessaging.requestToken.subscribe( (token) => {
            console.log(token);
        }, (err) => {
            console.error('Unable to get permission to notify.', err);
        });
    }

    receiveMessage(event) {
        if (event.data != null) {
            const type = event.data.firebaseMessaging.type;
            if (type == "push-received") {
                const firebaseMessagingData = event.data.firebaseMessaging.payload,
                      from = firebaseMessagingData.from,
                      payload = firebaseMessagingData.notification;

                if (from === environment.firebaseConfig.messagingSenderId) {
                    alert(payload.title + '\n' + payload.body);
                }
            }
        }
    }
}