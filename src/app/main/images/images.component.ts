import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { IImage } from 'src/app/models/image.model';
import { ImagesService } from 'src/app/services/images.service';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

  images$: Observable<IImage[]>

  constructor(private imagesService: ImagesService) { }

  ngOnInit(): void {
    this.images$ = this.imagesService.getImages();
  }

  deleteImage(id: string) {
    return this.imagesService.deleteImage(id).pipe(
      tap(() => this.images$ = this.images$.pipe(
        map(images => images.filter(image => image.id !== id))
      ))
    ).subscribe();
  }

}
