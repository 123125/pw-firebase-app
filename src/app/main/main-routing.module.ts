import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main.component';
import { CreateTransactionComponent } from './create-transaction/create-transaction.component';
import { TransactionsListComponent } from './transactions-list/transactions-list.component';
import { AddImagesComponent } from './add-images/add-images.component';
import { ImagesComponent } from './images/images.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'transactions',
        component: TransactionsListComponent
      },
      {
        path: 'create-transaction',
        component: CreateTransactionComponent
      },
      {
        path: 'add-images',
        component: AddImagesComponent
      },
      {
        path: 'images',
        component: ImagesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
