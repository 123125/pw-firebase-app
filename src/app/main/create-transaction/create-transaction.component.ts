import { Component, OnInit } from '@angular/core';
import { map, takeUntil, switchMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TransactionsService } from 'src/app/services/transactions.service';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.scss']
})
export class CreateTransactionComponent implements OnInit {

  public formGroup: FormGroup;
  public formBuilder: FormBuilder = new FormBuilder();

  onDestroy$ = new Subject();
  onSearch$ = new Subject();

  constructor(
    private _activatedRoute: ActivatedRoute,
    private transactionsService: TransactionsService
  ) {
    this.formGroup = this.formBuilder.group({userId: null, name: null, amount: null});
    this.name.setValidators([Validators.required]);
    this.amount.setValidators([Validators.required]);

    this._activatedRoute.queryParams.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(queryParams => {
      if (queryParams.username) this.name.setValue(queryParams.username);
      if (queryParams.amount) this.amount.setValue(queryParams.amount);
      if (queryParams.recipientId) this.userId.setValue(queryParams.recipientId);
    })
  }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this.onSearch$.complete();
  }

  submit() {
    this.transactionsService.createTransaction(this.userId.value, +this.amount.value).subscribe();
  }

  get userId(): AbstractControl {
    return this.formGroup.get('userId');
  }

  get name(): AbstractControl {
    return this.formGroup.get('name');
  }

  get amount(): AbstractControl {
    return this.formGroup.get('amount');
  }

}
