import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { concatMap, first, map, switchMap } from 'rxjs/operators';
import { IUserInfo } from '../models/user-info.model';
import { IUser } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private db: AngularFirestore,
    private auth: AngularFireAuth,
  ) {}

  addUser(uid: string, user: IUser): Observable<any> {
    return from( this.db.collection('users').doc(uid).set({...user, balance: 500}) );
  }

  getCurrentUser(): Observable<IUserInfo> {
    return this.auth.authState.pipe(
      concatMap(data => {
        return this.db.doc(`users/${data.uid}`).snapshotChanges()
      }),
      map( ((snap: any) => {
        return {
          id: snap.payload.id,
          ...snap.payload.data()
        }
      }))
    )
  }

  getUsersList(filterString: string): Observable<IUser[]> {
    this.auth.authState
    return this.auth.authState.pipe(
      switchMap(data => {
        return this.db.collection('users', ref => ref.orderBy("username").startAt(filterString).endAt(filterString+"\uf8ff")).get().pipe(
          map(snaps => snaps.docs),
          map( docs => docs.filter( (doc: any) => doc.id.toString() !==  data.uid.toString() ) ),
          map(docs => docs.map(doc => {
            return <IUser>{
                id: doc.id,
                name: doc.data()['username']
            }
          }))
        );
      }),
      first()
    )
  }

}
