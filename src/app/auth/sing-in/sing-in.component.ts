import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { SignInFormModel } from 'src/app/models/sign-in.model';

@Component({
  selector: 'app-sing-in',
  templateUrl: './sing-in.component.html',
  styleUrls: ['./sing-in.component.scss']
})
export class SingInComponent implements OnInit {

  public formGroup: FormGroup;

  public errorMessage$: BehaviorSubject<string> = new BehaviorSubject('');

  constructor(
    private formBuilder: FormBuilder,
    private auth: AngularFireAuth,
    private router: Router
  ) { 
    this.formGroup = this.formBuilder.group(new SignInFormModel());
    this.email.setValidators([Validators.required, Validators.email]);
    this.password.setValidators([Validators.required]);
  }

  ngOnInit(): void {}

  submit(e: Event) {
    this.auth.auth.signInWithEmailAndPassword(this.formGroup.value.email, this.formGroup.value.password)
    .then(response => {
      console.log(response);
      this.router.navigateByUrl('');
    }).catch(e => {
      console.log(e);
      this.errorMessage$.next(e.message);
      throw(e);
    });
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

}
