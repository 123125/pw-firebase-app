import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { UsersListComponent } from './users-list/users-list.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { TransactionsListComponent } from './transactions-list/transactions-list.component';
import { CreateTransactionComponent } from './create-transaction/create-transaction.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AddImagesComponent } from './add-images/add-images.component';
import { ImagesComponent } from './images/images.component';


@NgModule({
  declarations: [
    MainComponent,
    UsersListComponent,
    UserInfoComponent,
    TransactionsListComponent,
    CreateTransactionComponent,
    AddImagesComponent,
    ImagesComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    
    FormsModule,
    ReactiveFormsModule,
    
    BsDropdownModule.forRoot(),

  ]
})
export class MainModule { }
