import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { takeUntil, switchMap, takeWhile, tap, catchError } from 'rxjs/operators';
import { Subject, BehaviorSubject, timer, throwError, of, Observable } from 'rxjs';
import { IUser } from '../../models/user.model'
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @Output() getSelectUser = new EventEmitter<IUser>();

  @Input() selectedUser: IUser;

  usersList$: Observable<IUser[]>;
  
  getUsersListStatus$ = new BehaviorSubject<string>('');

  onDestroy$ = new Subject();

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  getUsersList(filter: string) {
    this.onDestroy$.next();
    timer(300).pipe(
      takeUntil(this.onDestroy$),
      tap(() => {
        if (filter === '') this.usersList$ = of([]);
      }),
      takeWhile(() => filter !== ''),
      tap(() => this.getUsersListStatus$.next('pending')),
      switchMap(() => {
        this.getUsersListStatus$.next('success');
        return this.usersList$ = this.usersService.getUsersList(filter);
      }),
      catchError(error => {
        this.getUsersListStatus$.next('error');
        this.usersList$ = of([]);
        return throwError(error);
      }),
    ).subscribe();
  }

  selectUser(user: IUser) {
    this.selectedUser = user;
    this.getSelectUser.emit(user);
  }
}
