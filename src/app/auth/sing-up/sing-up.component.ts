import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignUpFormModel } from 'src/app/models/sign-up.model';
import { BehaviorSubject, from, of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, concatMap } from 'rxjs/operators';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-sing-up',
  templateUrl: './sing-up.component.html',
  styleUrls: ['./sing-up.component.scss']
})
export class SingUpComponent implements OnInit {

  public formGroup: FormGroup;

  public errorMessage$: BehaviorSubject<string> = new BehaviorSubject('');

  constructor(
    private formBuilder: FormBuilder,
    private auth: AngularFireAuth,
    private router: Router,
    private usersService: UsersService
  ) { 
    this.formGroup = this.formBuilder.group(new SignUpFormModel());
    this.username.setValidators([Validators.required]);
    this.email.setValidators([Validators.required, Validators.email]);
    this.password.setValidators([Validators.required]);
  }

  ngOnInit(): void {}

  submit() {
    from(
      this.auth.auth.createUserWithEmailAndPassword(this.email.value, this.password.value)
    ).pipe(
      concatMap(response => {
        this.router.navigateByUrl('/auth/sign-in');
        const newUser = this.formGroup.value;
        delete newUser.password;
        return this.usersService.addUser(response.user.uid, newUser);
      }),
      catchError(e => {
        console.log(e);
        this.errorMessage$.next(e.message);
        throw(e);
      })
    ).subscribe();
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

}
