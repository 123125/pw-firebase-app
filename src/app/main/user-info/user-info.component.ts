import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { IUserInfo } from 'src/app/models/user-info.model';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  userInfo$: Observable<IUserInfo>;

  onDestroy$ = new Subject();

  constructor(
    private usersService: UsersService
  ) { }

  ngOnInit(): void {
    this.userInfo$ = this.usersService.getCurrentUser();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
