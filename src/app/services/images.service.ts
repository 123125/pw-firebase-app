import { Injectable } from "@angular/core";
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';
import { IImage } from '../models/image.model';

@Injectable({
    providedIn: 'root'
})
export class ImagesService {

  constructor(
    private db: AngularFirestore,
    private auth: AngularFireAuth,
  ) {}

  addImage(image: Partial<IImage>): Observable<any> {
    return this.auth.authState.pipe(
      switchMap(data => {
        return this.db.collection('images').add({...image, userId: data.uid})
      })
    )
  }
  
  getImages(): Observable<IImage[]> {
    return this.auth.authState.pipe(
      switchMap(data => {
        return this.db.collection('images', ref => ref.where('userId', '==', data.uid)).snapshotChanges()
      }),
      map(snaps => <any>snaps.map(snap => {
        return {
          ...(snap.payload.doc.data() as Partial<IImage>),
          id: snap.payload.doc.id
        }
      })),
      first()
    )
  }
  
  deleteImage(id: string): Observable<any> {
    return from(this.db.collection('images').doc(id).delete());
  }
}