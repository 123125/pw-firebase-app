importScripts('https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.5/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyBPVqC2WI4BqEdOlR2cTTUVRvbqpuDRZGQ",
    authDomain: "pw-app-firebase.firebaseapp.com",
    projectId: "pw-app-firebase",
    storageBucket: "pw-app-firebase.appspot.com",
    messagingSenderId: "376397046905",
    appId: "1:376397046905:web:0a4a89c4e6a8786a3f20a8",
    measurementId: "G-LBBVMFZVYW"
});

const messaging = firebase.messaging();
