export interface IImage {
    bucket: string,
    fullPath: string,
    name: string,
    userId: string,
    fullLink: string,
    id: string
}