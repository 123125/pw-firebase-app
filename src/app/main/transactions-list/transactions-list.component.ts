import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { TransactionsService } from 'src/app/services/transactions.service';
import { ITransaction } from '../../models/transaction.model';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss']
})
export class TransactionsListComponent implements OnInit {

  transactionsList$: Observable<ITransaction[]>;
  
  getTransactionsListStatus$;

  onDestroy$ = new Subject();

  constructor(private transactionsService: TransactionsService, private router: Router) { }

  ngOnInit(): void {
    this.getTransactions();
  }

  repeatTransaction(transaction: ITransaction) {
    this.router.navigate(['/create-transaction'],  {
      queryParams:{
          'username': transaction.username,
          'recipientId': transaction.recipientId,
          'amount': Math.abs(transaction.amount)
      }
    })
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  getTransactions(field?: string, direction?: string) {
    this.transactionsList$ = this.transactionsService.getTransactions(field, direction).pipe();
  }

  sortTransactions(type: string) {
    let field: string, direction: string;
    switch(type){
      case 'dateUp': field = 'date', direction = 'asc';
      break;
      
      case 'dateDown':  field = 'date', direction = 'desc';
      break;

      case 'nameUp': field = 'username', direction = 'asc';
      break;
      
      case 'nameDown': field = 'username', direction = 'desc';
      break;

      case 'amountUp': field = 'amount', direction = 'asc';
      break;
      
      case 'amountDown': field = 'amount', direction = 'desc';
      break;

      default: return;
    }
    this.getTransactions(field, direction);
  }

}
