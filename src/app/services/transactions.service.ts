import { Injectable } from "@angular/core";
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { concatMap, map, switchMap } from 'rxjs/operators';
import { ITransaction } from '../models/transaction.model';
import { IUserInfo } from '../models/user-info.model';

@Injectable({
    providedIn: 'root'
})
export class TransactionsService {

    constructor(
        private db: AngularFirestore,
        private auth: AngularFireAuth,
    ) {}

    createTransaction(recipientId: string, amount: number): Observable<any> {
        return this.auth.authState.pipe(
          concatMap(data => {
            return this.db.firestore.runTransaction(async fbTransaction => {
    
              // currentUser
              const currentUserRef = this.db.doc(`/users/${data.uid}`).ref;
              const currentUserSnap = await fbTransaction.get(currentUserRef);
              const currentUserData: IUserInfo = (currentUserSnap.data() as IUserInfo);
    
              // recipient
              const recipientRef = this.db.doc(`/users/${recipientId}`).ref;
              const recipientSnap = await fbTransaction.get(recipientRef);
              const recipientData: IUserInfo = (recipientSnap.data() as IUserInfo);
    
              // update currentUser balance
              await fbTransaction.update(currentUserRef, {
                balance: currentUserData.balance - amount
              });
    
              // update recipient balance
              await fbTransaction.update(recipientRef, {
                balance: recipientData.balance + amount
              });
    
              // collect transaction object
              return <ITransaction>{
                username: recipientData.username,
                balance: currentUserData.balance - amount,
                amount: amount,
                date: new Date().toISOString(),
                userId: data.uid,
                recipientId
              }
            });
          }),
          concatMap(data => {
            return from(this.db.collection('transactions').add({...data}));
          })
        )
    }
    
    getTransactions(field: string = 'date', direction: string = 'asc'): Observable<ITransaction[]> {
        return this.auth.authState.pipe(
          switchMap(data => {
            return this.db.collection('transactions', ref => ref.where('userId', '==', data.uid)
                                                                .orderBy(field, (direction as any))).snapshotChanges()
          }),
          map(snaps => snaps.map((snap: any) => {
            return <ITransaction>{
              id: snap.payload.doc.id,
              ...snap.payload.doc.data()
            }
          }))
        )
    }
}